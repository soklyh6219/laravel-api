<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoomtypeController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $roomType = Roomtype::with('image')->get();
        if($request->get('query'))
            $roomType = Roomtype::with('image')->where('title', 'like', '%' . $request->get('query') . '%')->get();
        return response()->json(['room_types' => $roomType], 200);
    }

    public function store(Request $request): JsonResponse
    {
        $validation = $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'bed_num' => 'required|integer',
            'people_num' => 'required|integer',
            'price' => 'required|numeric',
            'image' => 'required|array',
            'image.*' => 'required|image',
        ]);
        $roomType = Roomtype::create($validation);
        foreach ($validation['image'] as $imageFile){
            $imagePath = $imageFile->store('image', 'public');
            $roomType->image()->save(new Image(['path' => $imagePath]));
        }
        return response()->json([], 201);
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'bed_num' => 'required|integer',
            'people_num' => 'required|integer',
            'price' => 'required|numeric',
            'image' => 'required|array',
            'image.*' => 'required|image',
        ]);
        $room_type = RoomType::findOrFail($request->id);
        $room_type->fill($request->all())->save();
        return response()->json([], 201);
    }

    public function show($id): JsonResponse
    {
        $roomType = Roomtype::with('image')->where('id', $id)->first();
        return response()->json(['rooms' => $roomType], 200);
    }

    public function destroy($id): JsonResponse
    {
        Roomtype::destroy($id);
        return response()->json([], 200);
    }
}
