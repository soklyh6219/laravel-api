<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $users = User::withoutRole('admin')->with('roles')->get();

        if ($request->get('query')) {
            $users = User::withoutRole('admin')->where('name', 'LIKE', '%' . $request->get('query') . '%')->get();
        }

        return response()->json(['users' => $users], 200);
    }

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'role' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Password::defaults()],
        ]);
        $user = new User();
        $user->fill($request->all())->save();
        $user->assignRole($request->role);
        return response()->json(['user' => $user], 200);
    }

    public function update(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => ['required', 'string', 'max:255', Rule::unique(User::class)->ignore($request->id)],
            'role' => 'required|string|max:255',
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', Rule::unique(User::class)->ignore($request->id)],
        ]);

        $user = User::findOrFail($request->id);
        $user->fill($request->all())->save();

        if (!$user->hasRole($request->role))
            $user->syncRoles($request->role);

        return response()->json(['user' => $user], 200);
    }
    public function show($id): JsonResponse
    {
        return response()->json([
            'user' => User::with('roles')->find($id),
//            'roles' => Role::whereNotIn('name', ['Admin'])->pluck('name')
        ], 200);
    }
    public function destroy($id): JsonResponse
    {
        User::destroy($id);
        return response()->json([], 200);
    }

}
