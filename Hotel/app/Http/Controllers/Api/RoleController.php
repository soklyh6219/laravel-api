<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $roles = Role::with('permissions')->get();
        return response()->json(['roles' => $roles], 200);
    }

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', Rule::unique(Role::class)->ignore($request->id)],
        ]);
        $role = Role::create([
            'guard_name' => 'api',
            'name' => $request->name
        ]);
        $role->syncPermissions($request->permission);
        return response()->json([], 200);
    }

    public function update(Request $request): JsonResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', Rule::unique(Role::class)->ignore($request->id)],
        ]);

        $role = Role::findOrFail($request->id);
        $role->fill($request->all())->save();

        return response()->json(['role' => $role], 200);
    }

    public function show($id): JsonResponse
    {
        return response()->json([
            'role' => Role::with('permissions')->find($id),
        ], 200);
    }

    public function assign(Request $request, $id)
    {
        $role = Role::findById($id);
        $activePermission = [];
        $removePermission = [];

        foreach ($request->permission as $p){
            if($p['status']){
                $activePermission[] = $p['name'];
            }else{
                $removePermission[] = $p['name'];
            }
        }

        $role->givePermissionTo($activePermission);
        $role->revokePermissionTo($removePermission);

        return response()->json([
            'role' => Role::with('permissions')->find($id),
        ], 200);
    }

    public function destroy($id): JsonResponse
    {
        Role::destroy($id);
        return response()->json([], 200);
    }
}
