<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Room;
use App\Models\Roomtype;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoomController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $rooms = Room::with('roomtypes')->get();

        if($request->get('query'))
            $rooms = Room::with('roomtypes')->where('title', 'like', '%' . $request->get('query') . '%')->get();

        return response()->json(['rooms' => $rooms], 200);
    }

    public function store(Request $request): JsonResponse
    {
        $validate = $request->validate([
            'room_num' => ['required', 'integer', 'max:255', Rule::unique(Room::class)->ignore($request->id)],
            'floor_num' => 'required|integer|max:255',
            'status' => 'required|string|max:255',
        ]);

        $room = Room::create($validate);
        $roomType = Roomtype::findOrFail($request['room_type_id']);
        $roomType->room()->save($room);
        return response()->json([], 200);
    }

    public function update(Request $request): JsonResponse
    {
        $request->validate([
            'room_num' => ['required', 'string', 'max:255', Rule::unique(Room::class)->ignore($request->id)],
            'floor_num' => 'required|string|max:255',
            'status' => 'required|string|max:255',
        ]);

        $room = Room::findOrFail($request->id);
        $room->fill($request->all())->save();
        return response()->json(['room' => $room], 200);
    }

    public function show($id): JsonResponse
    {
        return response()->json([
            'room' => Room::with('roomtypes')->where('id', $id)->get(),
        ], 200);
    }

    public function destroy($id): JsonResponse
    {
        Room::destroy($id);
        return response()->json([], 200);
    }
}
