<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Room extends Model
{
    use HasFactory;
    protected $fillable = [
        'room_num',
        'floor_num',
        'status'
    ];

    public function roomType(): BelongsTo
    {
        return $this->belongsTo(Roomtype::class);
    }
}
