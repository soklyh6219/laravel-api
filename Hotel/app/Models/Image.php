<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Image extends Model
{
    use HasFactory;
    protected $table = 'image';
    protected $fillable = [
        'path',
    ];

    public function roomType(): BelongsTo
    {
        return $this->belongsTo(Roomtype::class);
    }
}
