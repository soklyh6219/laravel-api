<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Roomtype extends Model
{
    use HasFactory;
//    protected $table = 'roomtypes';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'desc',
        'bed_num',
        'people_num',
        'price',
    ];

    public function room(): HasMany
    {
        return $this->hasMany(Room::class);
    }

    public function image(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
