<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role_admin = Role::create(['guard_name' => 'api', 'name' => 'admin']);
        $role_standard = Role::create(['guard_name' => 'api', 'name' => 'User']);

        //permission user
        $permission_read = Permission::create(['guard_name' => 'api', 'name' => 'list-user']);
        $permission_edit = Permission::create(['guard_name' => 'api', 'name' => 'edit-user']);
        $permission_write = Permission::create(['guard_name' => 'api', 'name' => 'create-user']);
        $permission_delete = Permission::create(['guard_name' => 'api', 'name' => 'delete-user']);

        //permission role
        $role_read = Permission::create(['guard_name' => 'api', 'name' => 'list-role']);
        $role_edit = Permission::create(['guard_name' => 'api', 'name' => 'edit-role']);
        $role_write = Permission::create(['guard_name' => 'api', 'name' => 'create-role']);
        $role_delete = Permission::create(['guard_name' => 'api', 'name' => 'delete-role']);

        //permission room
        $room_read = Permission::create(['guard_name' => 'api', 'name' => 'list-room']);
        $room_edit = Permission::create(['guard_name' => 'api', 'name' => 'edit-room']);
        $room_write = Permission::create(['guard_name' => 'api', 'name' => 'create-room']);
        $room_delete = Permission::create(['guard_name' => 'api', 'name' => 'delete-room']);

        //permission room_type
        $room_type_read = Permission::create(['guard_name' => 'api', 'name' => 'list-room-type']);
        $room_type_edit = Permission::create(['guard_name' => 'api', 'name' => 'edit-room-type']);
        $room_type_write = Permission::create(['guard_name' => 'api', 'name' => 'create-room-type']);
        $room_type_delete = Permission::create(['guard_name' => 'api', 'name' => 'delete-room-type']);

        $permissions_admin = [
            $permission_read, $permission_edit, $permission_write, $permission_delete,
            $role_read, $role_edit, $role_write, $role_delete,
            $room_read, $room_edit, $room_write, $room_delete,
            $room_type_read, $room_type_edit, $room_type_write, $room_type_delete,
        ];

        $permission_user = [
            $permission_read,
            $room_read,
            $room_type_read,
        ];

        $role_admin->syncPermissions($permissions_admin);
        $role_standard->syncPermissions($permission_user);
    }
}
