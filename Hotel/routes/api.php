<?php

use App\Http\Controllers\Api\AuthenticationController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\RoomController;
use App\Http\Controllers\Api\RoomTypeController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('auth/register', [AuthenticationController::class, 'register']);
Route::post('auth/login', [AuthenticationController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::get('auth/profile', [AuthenticationController::class, 'profile']);
    Route::post('auth/logout', [AuthenticationController::class, 'logout']);

    //user management
    Route::get('user/index', [UserController::class, 'index'])
        ->middleware(['permission:list-user']);
    Route::post('user/create', [UserController::class, 'store'])
        ->middleware(['permission:create-user']);
    Route::get('user/show/{id}', [UserController::class, 'show'])
        ->middleware(['permission:list-user']);
    Route::put('user/update/{id}', [UserController::class, 'update'])
        ->middleware(['permission:edit-user']);
    Route::delete('user/delete/{id}', [UserController::class, 'destroy'])
        ->middleware(['permission:delete-user']);

    //role management
    Route::get('role/index', [RoleController::class, 'index'])
        ->middleware(['permission:list-role']);
    Route::post('role/create', [RoleController::class, 'store'])
        ->middleware(['permission:create-role']);
    Route::get('role/show/{id}', [RoleController::class, 'show'])
        ->middleware(['permission:list-role']);
    Route::put('role/update/{id}', [RoleController::class, 'update'])
        ->middleware(['permission:edit-role']);
    Route::delete('role/delete/{id}', [RoleController::class, 'destroy'])
        ->middleware(['permission:delete-role']);
    Route::put('role/assign/{id}', [RoleController::class, 'assign']);

    //room management
    Route::get('room/index', [RoomController::class, 'index'])
        ->middleware(['permission:list-room']);
    Route::post('room/create', [RoomController::class, 'store'])
        ->middleware(['permission:create-room']);
    Route::get('room/show/{id}', [RoomController::class, 'show'])
        ->middleware(['permission:list-room']);
    Route::put('room/update/{id}', [RoomController::class, 'update'])
        ->middleware(['permission:edit-room']);
    Route::delete('room/delete/{id}', [RoomController::class, 'destroy'])
        ->middleware(['permission:delete-room']);

    //room_type management
    Route::get('room-type/index', [RoomtypeController::class, 'index'])
        ->middleware(['permission:list-room-type']);
    Route::post('room-type/create', [RoomtypeController::class, 'store'])
        ->middleware(['permission:create-room-type']);
    Route::get('room-type/show/{id}', [RoomtypeController::class, 'show'])
        ->middleware(['permission:list-room-type']);
    Route::post('room-type/update/{id}', [RoomtypeController::class, 'update'])
        ->middleware(['permission:edit-room-type']);
    Route::delete('room-type/delete/{id}', [RoomtypeController::class, 'destroy'])
        ->middleware(['permission:delete-room-type']);
});
